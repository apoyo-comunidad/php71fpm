##############################################################
##          Instalacion de php 7.1-fpm sobre alpine         ##
##############################################################
FROM php:7.1.29-fpm-alpine
LABEL locopump = augusto.caceres.puma@gmail.com
RUN set -ex \
	&& apk --no-cache add postgresql-libs postgresql-dev \
	&& docker-php-ext-install pgsql pdo_pgsql \
	&& apk del postgresql-dev